import type { User } from "$lib/domains/RPS.type";
import { io, type Socket } from "socket.io-client";


let socket: Socket | null = null;

function initSocket() {
  const SOCKET_URL = import.meta.env.VITE_SOCKET_URL;
  console.log('SOCKET_URL', SOCKET_URL);
  socket = io(SOCKET_URL);

  socket.on("connect", () => {
    console.log("Connected to socket server");
  });

  const userId = localStorage.getItem('id');
  const userName = localStorage.getItem('name');
  const user: User = {
    id: userId || '',
    name: userName || '',
  };

  socket.emit('connect user', user.id, user.name);
  socket.on('connected', (user: User) => {
    localStorage.setItem('id', user.id);
    localStorage.setItem('name', user.name);
    console.log('Connected to socket server as', user.name)
  });

  socket.on("user not found", () => {
    if (userId && userName) {
      socket?.emit('connect user', userId, userName);
    }
  })


  return socket;
}

export function getSocket() {
  return socket || initSocket();
}