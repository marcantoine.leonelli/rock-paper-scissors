export type Choice = 'rock' | 'paper' | 'scissors' | null;

export type Result = 'win' | 'lose' | 'draw' | null;

export type OpponentType = 'computer' | 'player';

export type Game = {
  id: string;
  players: Player[];
  result: string | null;
}

export type Player = {
  id: string;
  name: string;
  score: number;
  choice: Choice;
  role: "host" | "guest";
}

export type User = {
  id: string;
  name: string;
}