import type { User } from '$lib/domains/RPS.type';
import type { Socket } from 'socket.io-client';

export default async function connectToSocket(socket: Socket): Promise<User | null> {
  return new Promise((resolve, reject) => {
    try {
      const userString = localStorage.getItem('user');
      let user: User | null = null;
      if (userString) {
        user = JSON.parse(userString);
      }
  
      socket.emit('connect user', user?.id, user?.name);
      socket.on('connected', (user: User) => {
        localStorage.setItem('user', JSON.stringify(user));
        resolve(user);
      });
    } catch (error) {
      reject(error);
    }
  });
}
